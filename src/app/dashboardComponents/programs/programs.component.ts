import { Component, OnInit ,ViewChild} from '@angular/core';
import{MatTableDataSource,MatSort,  MatPaginator  } from '@angular/material';
import{HomeService} from '../../home.service'
  import { from } from 'rxjs';
  

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.css']
})
export class ProgramsComponent implements OnInit {
  
  
  
  columnsToDisplay = ['id','program','duration'];
  
     public dataSource : any= [];


  @ViewChild(MatSort,{static: false}) sort: MatSort;
  @ViewChild(MatPaginator,{static: false}) paginator:MatPaginator; 
  constructor(private apiManager: HomeService ) { }

  logData(row){
    console.log(row)
  
  }

  ngOnInit() {
    this.getUsers();
    this.dataSource.sort=this.sort;
    this.dataSource.paginator=this.paginator
  }
  getUsers(){
    this.apiManager.getSchools("programs").subscribe((response)=>{
      this.dataSource=response
      console.log("response",this.dataSource)
    },
    error=>{
      console.log(error);
    })
  }

  applyFilter(filterValue:string){

    this.dataSource.filter=filterValue.trim().toLocaleLowerCase();
  
  }

}
