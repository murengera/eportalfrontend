import { Component, OnInit } from '@angular/core';
import { FormBuilder,Validators} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  constructor( private  fb:FormBuilder) { }
registrationForm=this.fb.group({
  name:[' ',Validators.required],
  email:[],
  password:[],
  confirmPassword:[]
})
  

  ngOnInit() {
  }

}
